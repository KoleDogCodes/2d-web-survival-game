var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    },
    scale: {
        mode: Phaser.Scale.RESIZE,
    }
};

var game = new Phaser.Game(config);

function preload() {
    //Entities
    this.load.image('player', './assets/player.png');

    //Blocks
    this.load.image('grass', './assets/grass.png');
    this.load.image('dirt', './assets/dirt.png');
    this.load.image('stone', './assets/stone.png');
    this.load.image('air', './assets/air.png');
}



function create() {
    //Intilase Game Constants
    BLOCKSIZE = 64;
    cursor = null;
    const WORLD_WIDTH = 1000000;
    const WORLD_HEIGHT = 75;

    //Set game world bounds
    this.physics.world.setBounds(-(BLOCKSIZE * WORLD_WIDTH), -(BLOCKSIZE * WORLD_HEIGHT / 2), BLOCKSIZE * WORLD_WIDTH, BLOCKSIZE * (WORLD_HEIGHT));

    //Create the world generator object
    newWorld = new WorldGenerator(this, 1573902040593);
    newWorld.setWorldSize(WORLD_WIDTH, WORLD_HEIGHT);

    //Generate terrian
    newWorld.generateChunk(-1);
    newWorld.generateChunk(0);
    newWorld.generateChunk(1);

    //Create player and set colliders for player
    player = new Player(this, newWorld.getSpawnLocation().x, newWorld.getSpawnLocation().y - 100);
    player.setWorld(newWorld);
    prevChunkId = 0;

    //Setup camera to follow player
    this.cameras.main.startFollow(player.sprite);

    //Create key object for events
    SpcKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    AKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    DKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);

    player.mouse_listener();
    player.keyboard_listener();
    newWorld.setColliders(player);

    //HUD
    txt_loc = this.add.text(32, 16, ``, { fontFamily: '"Roboto Condensed"' });
    txt_loc.setScrollFactor(0);
}

function getColor(hex) {
    return Phaser.Display.Color.HexStringToColor(hex).color;
}

function update() {
    player.update();

    if (prevChunkId != newWorld.getCurrentChunk(player)) {
        currentChunkId = newWorld.getCurrentChunk(player);
        console.log(`Chunk ID: ${prevChunkId} -> ${currentChunkId}`);

        //Delete previous terrian from memory ()
        if (prevChunkId < currentChunkId) {
            newWorld.blocks[prevChunkId - 1].clear(true, true);
            delete newWorld.blocks[prevChunkId - 1];
        }
        else if (prevChunkId > currentChunkId) {
            newWorld.blocks[prevChunkId + 1].clear(true, true);
            delete newWorld.blocks[prevChunkId + 1];
        }

        prevChunkId = currentChunkId;

        //Generate new surronding terrian
        if (newWorld.blocks[currentChunkId - 1] == null) { newWorld.generateChunk(currentChunkId - 1) }
        if (newWorld.blocks[currentChunkId] == null) { newWorld.generateChunk(currentChunkId) }
        if (newWorld.blocks[currentChunkId + 1] == null) { newWorld.generateChunk(currentChunkId + 1) }

        //Set new colliders for new terrian
        newWorld.setColliders(player);
    }

    txt_loc.setText(`X: ${Math.floor(player.sprite.x / BLOCKSIZE)} | Y: ${Math.floor(player.sprite.y / BLOCKSIZE)} | Chunk ID: ${newWorld.getCurrentChunk(player)}`);
}