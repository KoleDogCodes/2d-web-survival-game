class WorldGenerator {

    constructor(pharser, seed) {
        this.seed = seed;
        this.pharser = pharser;
        this.blocks = {};
        this.spawnLocation = null;

        if (localStorage.getItem('world') == null) { localStorage.setItem('world', JSON.stringify({})); }

        this.worldData = JSON.parse(localStorage.getItem('world'));
    }

    getRandom(seed, val, range) {
        Math.seedrandom(seed);
        return (Math.random() * val);
    }


    setWorldSize(width, height) {
        this.worldSize = width;
        this.worldHeight = height;
    }

    getSpawnLocation() {
        return this.spawnLocation;
    }

    noise(x) {
        const perlin1 = new tumult.Perlin1(this.seed);
        const noiseFactor = this.worldHeight / (200 / 500);
        return perlin1.gen(x / noiseFactor);
    }

    fillColumn(chunkId, x, y) {
        var type = 'dirt';
        const STONE_LEVEL = 3;
        for (var gridY = y; gridY <= this.worldHeight; gridY++) {

            if (gridY >= (y + STONE_LEVEL)) { type = 'stone' }

            if (this.worldData[`CHUNK_${chunkId}`][`X_${x}`][`Y_${gridY}`] != null) {
                type = this.worldData[`CHUNK_${chunkId}`][`X_${x}`][`Y_${gridY}`].type;
                console.log(`Typed block at: (${x}, ${gridY})`);
            }

            this.placeBlock(chunkId, x, gridY, type);
        }
    }

    placeBlock(id, x, y, type) {
        const Y_MIDDLE = (this.worldHeight / 2) * BLOCKSIZE;

        const worldX = (x * BLOCKSIZE) + BLOCKSIZE / 2;
        const worldY = (Y_MIDDLE + (y * BLOCKSIZE)) + BLOCKSIZE / 2;

        var block;

        if (type == 'air') {
            console.warn(`Skipped placement of air blocks @ (${x}, ${y})`);
            return true;
        }

        block = this.pharser.physics.add.sprite(worldX, worldY, type);
        block.setInteractive();


        if (this.blocks[id.toString()] == null || this.blocks[id.toString()] == undefined) {
            this.blocks[id.toString()] = this.pharser.physics.add.group({
                immovable: true
            });
        }

        this.blocks[id.toString()].add(block);

        //Add block to world data save 
        if (this.worldData[`CHUNK_${id}`] == null) { this.worldData[`CHUNK_${id}`] = {}; }
        if (this.worldData[`CHUNK_${id}`][`X_${x}`] == null) { this.worldData[`CHUNK_${id}`][`X_${x}`] = {}; }
        if (this.worldData[`CHUNK_${id}`][`X_${x}`][`Y_${y}`] == null) { this.worldData[`CHUNK_${id}`][`X_${x}`][`Y_${y}`] = {}; }
        this.worldData[`CHUNK_${id}`][`X_${x}`][`Y_${y}`] = { type };

        if (x == 3 && this.spawnLocation == null) {
            this.spawnLocation = { x: x * BLOCKSIZE, y: (Y_MIDDLE + (y * BLOCKSIZE)) }
            //console.log(`World spawn location at: (${this.spawnLocation.x}, ${this.spawnLocation.y})`);
        }
    }

    removeBlock(block) {
        const blockChunkId = this.getBlockChunk(block);

        const gridX = Math.floor(block.x / BLOCKSIZE),
            gridY = Math.floor(block.y / BLOCKSIZE);

        this.worldData[`CHUNK_${blockChunkId}`][`X_${gridX}`][`Y_${gridY}`] = { type: 'air' };

        console.log(`ID: ${blockChunkId} @ (${gridX}, ${gridY})`);

        localStorage.setItem('world', JSON.stringify(this.worldData));

        this.blocks[blockChunkId.toString()].remove(block, true);
        block.destroy(false);
    }

    getCurrentChunk(player) {
        return Math.ceil((player.sprite.x / CHUNKSIZE) / CHUNKSIZE / 2.6) - 1;
    }

    getBlockChunk(block) {
        return Math.ceil((block.x / CHUNKSIZE) / CHUNKSIZE / 2.6) - 1;
    }

    generateNegativeChunk(dchunkId) {
        const xoffIncr = 3.5;

        const chunkId = -dchunkId;
        var xoff = xoffIncr * CHUNKSIZE * (chunkId - 1);

        if (dchunkId == -1) { xoff = 0; }

        var startX = chunkId == -1 ? 0 : ((-chunkId) * CHUNKSIZE) * BLOCKSIZE;
        var endX = chunkId == -1 ? -CHUNKSIZE * BLOCKSIZE : startX + (CHUNKSIZE * 64);

        console.log(`STARTING ----- Chunk ID: -${chunkId} | StartX: ${Math.max(startX, endX)} & EndX: ${Math.min(startX, endX)} | XOff: ${xoff}`);

        for (var x = Math.max(startX, endX) / BLOCKSIZE; x >= Math.min(startX, endX) / BLOCKSIZE; x--) {
            const noise = this.noise(xoff);
            var y = Math.floor((noise * (this.worldHeight / 2)));
            xoff += xoffIncr;
            this.placeBlock(-chunkId, x, y, 'grass');
            this.fillColumn(-chunkId, x, y + 1);
            console.log(`(${x}, ${y}, N: ${noise})`);
        }

        console.log(`ENDING ----- Chunk ID: -${chunkId} | StartX: ${Math.max(startX, endX)} & EndX: ${Math.min(startX, endX)} | XOff: ${xoff}`);
    }

    generateChunk(chunkId) {
        const chunkData = this.worldData[`CHUNK_${chunkId}`];
        if (chunkData != null) {
            console.log(`Loading chunk '${chunkId}' from world save...`);
            const chunkColumns = Object.keys(chunkData);
            
            for (var i = 0; i < chunkColumns.length; i++) {
                const chunkColumnBlocks = Object.keys(chunkData[chunkColumns[i]]);
                
                for (var z = 0; z < chunkColumnBlocks.length; z++) {
                    const x = chunkColumns[i], y = chunkColumnBlocks[z];

                    const gridX = x.replace("X_", "");
                    const gridY = y.replace("Y_", "");
                    const type = chunkData[x][y].type;

                    this.placeBlock(chunkId, gridX, gridY, type);
                }
            }

            console.log(`Chunk '${chunkId}' data from world save succesfully loaded.`);
            return;
        }

        console.log(`No world save for: ${chunkId}...Generating new terrian`);


        if (chunkId < 0) {
            this.generateNegativeChunk(chunkId);
            return;
        }

        const xoffIncr = 3;
        var xoff = chunkId > 0 ? 1 + ((CHUNKSIZE * xoffIncr) * chunkId) : 0;

        var startX = chunkId == 0 ? 0 : ((chunkId) * CHUNKSIZE) * BLOCKSIZE;
        var endX = chunkId == 0 ? CHUNKSIZE * BLOCKSIZE : startX + (CHUNKSIZE * 64);
        //console.log(`Chunk ID: ${chunkId} | StartX: ${startX} & EndX: ${endX}`);

        for (var x = startX / BLOCKSIZE; x <= endX / BLOCKSIZE; x++) {
            const noise = this.noise(xoff);
            var y = Math.floor((noise * (this.worldHeight / 2)));
            xoff += xoffIncr;
            this.placeBlock(chunkId, x, y, 'grass');
            this.fillColumn(chunkId, x, y + 1);
            //console.log(`(${x}, ${y}, N: ${noise})`);
        }

        localStorage.setItem('world', JSON.stringify(this.worldData));
    }

    setColliders(player) {
        Object.keys(this.blocks).forEach(id => {
            this.pharser.physics.add.collider(player.sprite, this.blocks[id]);
        });
    }

}

const CHUNKSIZE = 25;